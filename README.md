# Translation-Gummy Runner

## How to use

To create translated PDF, run as below.

```
docker run --rm -v $PWD:/work -w /work gummy gummy-journal -O /work "https://www.nature.com/articles/s41467-019-08316-9"
```

