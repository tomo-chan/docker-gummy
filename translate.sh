#!/bin/sh

URL=$1

set -eu

docker run --rm -v $PWD:/work -w /work gummy gummy-journal -O /work "${URL}"
