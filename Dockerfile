FROM ubuntu:focal-20200729

LABEL maintainer="tomo-chan <aradgyma@gmail.com>"

ARG PYTHON_VER=3.8
ARG WKHTMLTOPDF_VER=0.12.5
RUN apt-get update && \
    apt-get -y --no-install-recommends install curl ca-certificates \
       python3=${PYTHON_VER}* \
       python3-dev=${PYTHON_VER}* \
       wkhtmltopdf=${WKHTMLTOPDF_VER}* && \
    rm -rf /var/lib/apt/lists/*

ARG PIP_VER=20.2.2
ARG GUMMY_VER=3.4.3
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && \
    python3 get-pip.py pip==${PIP_VER} && \
    pip3 install Translation-Gummy==${GUMMY_VER}

# Install google chrome driver for Selenium
ARG CHROME_VER=85.0.4183.83
RUN apt-get update && \
    apt-get install -y --no-install-recommends unzip xvfb libxi6 libgconf-2-4 gnupg2 && \
    curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add && \
    echo "deb [arch=amd64]  http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list && \
    apt-get update && \
    apt-get -y --no-install-recommends install google-chrome-stable=${CHROME_VER}* && \
    rm -rf /var/lib/apt/lists/*
COPY chromedriver/chromedriver_linux64_${CHROME_VER} /usr/local/bin/chromedriver
RUN chmod +x /usr/local/bin/chromedriver

# License: https://moji.or.jp/ipafont/license/
RUN mkdir /root/.fonts
COPY ipafont/IPAexfont00401 /root/.fonts/
RUN fc-cache -fv

WORKDIR /work