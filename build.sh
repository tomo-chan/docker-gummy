#!/bin/sh

IMAGE_NAME=$1

set -ue

docker build -t "${IMAGE_NAME}" .